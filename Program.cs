using System;
using DataStructure;

class Program
{
    static void Main()
    {
        const int sizeOfList = 10;
        
        // Створення
        List list = new List();

        // Заповнення
        for (int i = 0; i < sizeOfList; i++)
        {
            list.Add(GetRandomNumber());
        }
        Console.WriteLine(list);
        
        
        // Новий список зі значеннями більше, ніж вказане
        List newList = list.GetListWithValuesGreaterThan(500);
        Console.WriteLine(newList);

        // Зміна значень за допомогою індексатора
        list[0] = 1;
        list[1] = 2;
        list[3] = 9;

        // Пошук елемента, кратного даному значенню
        short? value = list.FindFirstMultiple(10);
        if (value == null)
        {
            Console.WriteLine("No elements");
        }
        else
        {
            Console.WriteLine(value);
        }
        
        // Вставка "0" замість парних елементів
        list.ReplaceEvenPositionsWithZero();
        Console.WriteLine(list);
        
        // Видалення елементів, що знаходяться на непарних позиціях
        list.RemoveOddPositionedElements();
        Console.WriteLine(list);
    }
    
    public static short GetRandomNumber()
    {
        Random random = new Random();
        return (short) random.Next(-32768, 32767);
    }
}