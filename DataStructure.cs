using System.Text;

namespace DataStructure
{
    public class Node
    {
        public short Data { get; set; }
        public Node Next { get; set; }

        public Node(short data)
        {
            Data = data;
        }
    }

    public class List
    {
        private Node head;

        public List()
        {
            head = null;
        }

        public short this[int index]
        {
            get { return GetNodeAt(index).Data; }
            set
            {
                Node node = GetNodeAt(index);
                if (node != null)
                {
                    node.Data = value;
                }
                else
                {
                    throw new IndexOutOfRangeException("Index out of range");
                }
            }
        }
    
        private Node GetNodeAt(int index)
        {
            Node current = head;
            int count = 0;
        
            while (current != null)
            {
                if (count == index)
                    return current;
                current = current.Next;
                count++;
            }
            throw new IndexOutOfRangeException("Index out of range");
        }

        // Додавання нового елементу до кінця списку
        public void Add(short data)
        {
            Node newNode = new Node(data);
            if (head == null)
            {
                head = newNode;
            }
            else
            {
                Node current = head;
                while (current.Next != null)
                {
                    current = current.Next;
                }
                current.Next = newNode;
            }
        }

        // 1. Знайти елемент, кратний заданому значенню
        public short? FindFirstMultiple(short value)
        {
            Node current = head;
            while (current != null)
            {
                if (current.Data % value == 0)
                    return current.Data;
                current = current.Next;
            }
            return null;
        }

        // 2. Замінити елементи на парних позиціях на "0"
        public void ReplaceEvenPositionsWithZero()
        {
            Node current = head;
            int index = 1; // Починаємо з 1
            while (current != null)
            {
                if (index % 2 == 0)
                {
                    current.Data = 0;
                }
                current = current.Next;
                index++;
            }
        }

        // 3. Отримати новий список зі значень елементів більших за задане значення
        public List GetListWithValuesGreaterThan(short value)
        {
            List newList = new List();
            Node current = head;
            while (current != null)
            {
                if (current.Data > value)
                {
                    newList.Add(current.Data);
                }
                current = current.Next;
            }
            return newList;
        }

        // 4. Видалити елементи на непарних позиціях
        public void RemoveOddPositionedElements()
        {
            if (head == null || head.Next == null)
                return;

            Node prev = null;
            Node current = head;
            int index = 1; // Починаємо з 1

            while (current != null)
            {
                if (index % 2 != 0)
                {
                    if (prev == null)
                    {
                        head = current.Next;
                    }
                    else
                    {
                        prev.Next = current.Next;
                    }
                }
                else
                {
                    prev = current;
                }
                current = current.Next;
                index++;
            }
        }
    
        public int Length()
        {
            int count = 0;
            Node current = head;
            while (current != null)
            {
                count++;
                current = current.Next;
            }
            return count;
        }
    
        public override string ToString()
        {
            Node current = head;
            StringBuilder result = new StringBuilder();
            while (current != null)
            {
                result.Append(current.Data + " ");
                current = current.Next;
            }
            return result.ToString();
        }
    }
}

